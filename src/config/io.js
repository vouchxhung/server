module.exports = (app, http) => {
    app.io = require('socket.io')(http, {
        cors: {
            origin: process.env.CORS,
            methods: ["GET", "POST"]
        }
    });
    app.io.on('connection', (socket) => {
        // console.log('User connected', socket.id);

        socket.on('joinRoom', data => {
            const { username, roomId } = data
            socket.join(roomId)
            app.model.Mapping.create({
                socketId: socket.id,
                username,
                roomId
            }, (error) => {
                socket.emit('connected', { error, username, roomId })
            });
        })

        socket.on('chat', data => {
            const _data = {
                username: data.username,
                roomId: data.roomId,
                content: data.message
            }
            app.model.Message.create(_data, (error, res) => {
                if (error) return
                app.io.to(_data.roomId).emit("message", res._doc)
            })
        })

        socket.on('disconnect', () => {
            app.model.Mapping.logout({
                socketId: socket.id,
            }, (error) => {});
        });
    });
};
