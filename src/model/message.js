module.exports = function (app) {
  const schema = app.db.Schema({
    username: { type: String, required: true },
    roomId: { type: String, required: true },
    content: { type: String, required: true }
  },  { timestamps: true });

  const model = app.db.model('message', schema);
  app.model.Message = {
    getAllByRoom: function (data, done) {
      return model.find({
        roomId: data.roomId
      }, null, {
        sort: { createdAt: -1},
        limit: data.query?.pageSize || 0,
        skip: data.query?.offset || 0
      }, done)
    },
    create: function (data, done) {
      model.create(data, done);
    },
  };
};
