module.exports = function (app) {
  const schema = app.db.Schema({
    socketId: String,
    username: String,
    roomId: String,
  });
  schema.index({username: 1, roomId: 1}, {unique: true});

  const model = app.db.model('mapping', schema);
  app.model.Mapping = {
    create: function (data, done) {
      model.create(data, function (error, user) {
        done(error, user);
      });
    },

    logout: function (data, done) {
      model.deleteOne({ socketId: data.socketId }, function (error) {
        done(error);
      });
    },
  };

  model.find({}).deleteMany().exec()
};
