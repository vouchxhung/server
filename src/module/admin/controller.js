module.exports = (app) => {
    app.rooms = {}
    app.get('/message/', (req, res) => {
        app.model.Message.getAllByRoom({
            roomId: req.query.roomId,
            query: {
                pageSize: Number(req.query.pageSize),
                offset: Number(req.query.offset),
            }
        }, (error, data) => {
            return res.send({ error, data })
        })
    })
};
