FROM node:14.17.0-alpine3.13

ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "./"]

RUN npm install --production

COPY . .

EXPOSE 8000

CMD [ "node", "index.js" ]
