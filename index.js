require('dotenv').config()
const express = require('express');
const app = express();
const http = require('http').Server(app);

// constiables ==================================================================
app.title = 'Vouch interview';
app.version = '1.0.1';
app.isDebug = true;
app.mongodb = process.env.DATABASE_URI;
app.path = require('path');
app.mailFrom = 'leviethung2403@gmail.com';
app.mailPassword = 'osa@2013';
app.mailCc = [];
app.viewPath = app.path.join(__dirname, 'src/view') + '/';
app.modelPath = app.path.join(__dirname, 'src/model') + '/';
app.modulePath = app.path.join(__dirname, 'src/module') + '/';
app.publicPath = app.path.join(__dirname, 'public');
app.faviconPath = app.path.join(__dirname, 'public/favicon.ico');

// Launch website =============================================================
const port = process.env.PORT || 8000;
const server = http.listen(port, () => {
    console.log('The magic happens on http://localhost:%d.', port);
});

// Configure ==================================================================
require('./src/config/common')(app);
require('./src/config/packages')(app);
require('./src/config/database')(app);
// require('./src/config/authentication')(app);
require('./src/config/view')(app, express);
// require('./src/config/schedule')(app);
require('./src/config/io')(app, server);
//require('./src/config/error')(app);

// Load project's components ==================================================
app.loadModels();
app.loadModules();
